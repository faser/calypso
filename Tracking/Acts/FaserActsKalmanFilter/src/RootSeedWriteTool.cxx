#include"RootSeedWriteTool.h"

#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "Identifier/Identifier.h"
#include "TrackClassification.h"
#include "GeoPrimitives/GeoPrimitives.h"
//#include "Acts/EventData/MultiTrajectory.hpp"
//#include "Acts/EventData/MultiTrajectoryHelpers.hpp"

#include "Acts/EventData/detail/TransformationBoundToFree.hpp"
#include "Acts/Utilities/Helpers.hpp"

//#include "GeoPrimitives/CLHEPtoEigenConverter.h"
#include "TrackerReadoutGeometry/SiDetectorElement.h"
#include "xAODTruth/TruthParticle.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"

#include <TFile.h>
#include <TTree.h>
#include<string>
RootSeedWriterTool::RootSeedWriterTool(
    const std::string& type, const std::string& name, const IInterface* parent)
    : AthAlgTool(type, name, parent) {}

/// NaN values for TTree variables
//constexpr float NaNfloat = std::numeric_limits<float>::quiet_NaN();
constexpr float NaNfloat = std::numeric_limits<float>::quiet_NaN();
constexpr float NaNdouble = std::numeric_limits<double>::quiet_NaN();
constexpr int NaNint = std::numeric_limits<int>::quiet_NaN();
using Acts::VectorHelpers::eta;
using Acts::VectorHelpers::perp;
using Acts::VectorHelpers::phi;
using Acts::VectorHelpers::theta;


StatusCode RootSeedWriterTool::initialize() {
//  ATH_CHECK(detStore()->retrieve(m_idHelper, "FaserSCT_ID"));
  ATH_CHECK(m_mcEventCollectionKey.initialize());
  ATH_CHECK(m_simDataCollectionKey.initialize());
  ATH_CHECK(m_fiducialParticleTool.retrieve());

  if (!m_noDiagnostics) {
    std::string filePath = m_filePath;
    std::string treeName = m_treeName;
    m_outputFile = TFile::Open(filePath.c_str(), "RECREATE");
    if (m_outputFile == nullptr) {
      ATH_MSG_WARNING("Unable to open output file at " << m_filePath);
      return StatusCode::RECOVERABLE;
    }
    m_outputFile->cd();
    m_outputTree = new TTree(treeName.c_str(), treeName.c_str());
    if (m_outputTree == nullptr) {
      ATH_MSG_ERROR("Unable to create TTree");
      return StatusCode::FAILURE;
    }
    
    
    m_outputTree->Branch("truth_p", &m_truth_p);
    m_outputTree->Branch("truth_vx", &m_truth_vx);
    m_outputTree->Branch("truth_vy", &m_truth_vy);
    m_outputTree->Branch("truth_vz", &m_truth_vz);
    m_outputTree->Branch("truth_px", &m_truth_px);
    m_outputTree->Branch("truth_py", &m_truth_py);
    m_outputTree->Branch("truth_pz", &m_truth_pz);
    m_outputTree->Branch("truth_pT", &m_truth_pT);
//    m_outputTree->Branch("truth_pdg", &m_truth_pdg);
    m_outputTree->Branch("truth_barcode", &m_truth_barcode);
    m_outputTree->Branch("truth_theta", &m_truth_theta);
    m_outputTree->Branch("truth_phi", &m_truth_phi);
    m_outputTree->Branch("truth_eta", &m_truth_eta);
    m_outputTree->Branch("truth_isFiducial", &m_truth_isFiducial);

    m_outputTree->Branch("event_number", &m_eventNr);
    m_outputTree->Branch("targetZPosition", &m_targetZPosition);

    m_outputTree->Branch("nSeeds", &m_nseeds);
    m_outputTree->Branch("nClusters", &m_nClusters);
    m_outputTree->Branch("nMeasurements", &m_nMeasurements);
    m_outputTree->Branch("nSegments", &m_nSegments);
    m_outputTree->Branch("Positionx", &m_positionX);
    m_outputTree->Branch("Positiony", &m_positionY);
    m_outputTree->Branch("Positionz", &m_positionZ);
    m_outputTree->Branch("Spacepoint_x", &m_spacePointX);
    m_outputTree->Branch("Spacepoint_y", &m_spacePointY);
    m_outputTree->Branch("Spacepoint_z", &m_spacePointZ);
    m_outputTree->Branch("Fakeposition_x", &m_fakePositionX);
    m_outputTree->Branch("FakePosition_y", &m_fakePositionY);
    m_outputTree->Branch("FakePosition_z", &m_fakePositionZ);

    m_outputTree->Branch("nMajorityHits", &m_nMajorityHits);
    m_outputTree->Branch("majorityParticleId", &m_majorityParticleId);
    m_outputTree->Branch("t_truthHitRatio", &m_t_truthHitRatio);
    //m_outputTree->Branch("t_charge", &m_t_charge);
    //m_outputTree->Branch("t_time", &m_t_time);

    m_outputTree->Branch("track_loc0", &m_track_loc0);
    m_outputTree->Branch("track_loc1", &m_track_loc1);
    m_outputTree->Branch("track_theta", &m_track_theta);
    m_outputTree->Branch("track_phi", &m_track_phi);
    m_outputTree->Branch("track_QoverP", &m_track_QoverP);
    m_outputTree->Branch("track_time", &m_track_time);
    
    m_outputTree->Branch("track_vx", &m_track_vx);
    m_outputTree->Branch("track_vy", &m_track_vy);
    m_outputTree->Branch("track_vz", &m_track_vz);
    m_outputTree->Branch("track_px", &m_track_px);
    m_outputTree->Branch("track_py", &m_track_py);
    m_outputTree->Branch("track_pz", &m_track_pz);
    m_outputTree->Branch("track_eta", &m_track_eta);
    m_outputTree->Branch("track_p", &m_track_p);
    m_outputTree->Branch("track_pT", &m_track_pT);
    
    m_outputTree->Branch("t_loc0", &m_t_loc0);
    m_outputTree->Branch("t_loc1", &m_t_loc1);
    m_outputTree->Branch("t_theta", &m_t_theta);
    m_outputTree->Branch("t_phi", &m_t_phi);
    m_outputTree->Branch("t_QoverP", &m_t_QoverP);
    m_outputTree->Branch("t_time", &m_t_time);
    
    m_outputTree->Branch("t_vx", &m_t_vx);
    m_outputTree->Branch("t_vy", &m_t_vy);
    m_outputTree->Branch("t_vz", &m_t_vz);
    m_outputTree->Branch("t_px", &m_t_px);
    m_outputTree->Branch("t_py", &m_t_py);
    m_outputTree->Branch("t_pz", &m_t_pz);
    m_outputTree->Branch("t_eta", &m_t_eta);
    m_outputTree->Branch("t_p", &m_t_p);
    m_outputTree->Branch("t_pT", &m_t_pT);


  }
  return StatusCode::SUCCESS;
}
StatusCode RootSeedWriterTool::finalize() {
  if (!m_noDiagnostics) {
    m_outputFile->cd();
    m_outputTree->Write();
    m_outputFile->Close();
  }
  return StatusCode::SUCCESS;
}


StatusCode RootSeedWriterTool::write(const Acts::GeometryContext& geoContext, const double &targetZPosition, const Acts::BoundSquareMatrix& cov, const std::vector<CircleFitTrackSeedTool::Seed> &seeds, bool isMC, bool backward) const {
  EventContext ctx = Gaudi::Hive::currentContext();

  std::shared_ptr<TrackerSimDataCollection> simData {nullptr};
  std::map<int, const HepMC::GenParticle*> particles {};
  if (isMC) {

    SG::ReadHandle<TrackerSimDataCollection> simDataHandle {m_simDataCollectionKey, ctx};
    ATH_CHECK(simDataHandle.isValid());
    simData = std::make_shared<TrackerSimDataCollection>(*simDataHandle);
    
    SG::ReadHandle<McEventCollection> mcEvents {m_mcEventCollectionKey, ctx};
    ATH_CHECK(mcEvents.isValid());
    if (mcEvents->size() != 1) {
      ATH_MSG_ERROR("There should be exactly one event in the McEventCollection.");
      return StatusCode::FAILURE;
    }
    for (const auto& particle : mcEvents->front()->particles()) {
      particles[HepMC::barcode(particle)] = &(*particle);
       
      

       bool isFiducial = m_fiducialParticleTool->isFiducial(HepMC::barcode(particle));
  //     m_truth_pdg.push_back(particle->pdgId());
      
       m_truth_isFiducial.push_back(isFiducial); 
       if(isFiducial){

        float truth_vx = NaNfloat;
        float truth_vy = NaNfloat;
        float truth_vz = NaNfloat;
        float truth_px = NaNfloat;
        float truth_py = NaNfloat;
        float truth_pz = NaNfloat;
        float truth_theta = NaNfloat;
        float truth_phi = NaNfloat;
        float truth_eta = NaNfloat;
        float truth_p = NaNfloat;
        float truth_pT = NaNfloat;

        const HepMC::GenParticle* truthparticle = &(*particle);
        std::optional<const Acts::BoundTrackParameters> truthParameters
               = extrapolateToReferenceSurface(ctx, truthparticle, targetZPosition);
        if(!truthParameters){
          continue;
        }
	
	m_truth_barcode.push_back(HepMC::barcode(particle));
        truth_p = truthParameters->momentum().norm();
       //  m_truth_charge = truthParameters->charge();
       //  m_truth_time = truthParameters->time();
        truth_vx = truthParameters->position(geoContext).x();
        truth_vy = truthParameters->position(geoContext).y();
        truth_vz = truthParameters->position(geoContext).z();
        truth_px = truthParameters->momentum().x();
        truth_py = truthParameters->momentum().y();
        truth_pz = truthParameters->momentum().z();
        truth_theta = theta(truthParameters->momentum().normalized());
        truth_phi = phi(truthParameters->momentum().normalized());
        truth_eta = eta(truthParameters->momentum().normalized());
        truth_pT = truth_p * perp(truthParameters->momentum().normalized());

        m_truth_vx.push_back(truth_vx);
        m_truth_vy.push_back(truth_vy);
        m_truth_vz.push_back(truth_vz);

        m_truth_p.push_back(truth_p);
        m_truth_px.push_back(truth_px);
        m_truth_py.push_back(truth_py);
        m_truth_pz.push_back(truth_pz);
        m_truth_pT.push_back(truth_pT);
        m_truth_theta.push_back(truth_theta);
        m_truth_phi.push_back(truth_phi);
        m_truth_eta.push_back(truth_eta);
      }
    }
  }
  
  // For each particle within a track, how many hits did it contribute
  std::vector<ParticleHitCount> particleHitCounts;

  // Loop over the seeds from CircleFitTrackSeedTool
  m_eventNr = ctx.eventID().event_number();
  m_targetZPosition = targetZPosition;
  m_nseeds = seeds.size();
  for(auto seed : seeds){
    m_nClusters.push_back(seed.clusters.size());
    m_nMeasurements.push_back(seed.Measurements.size());
    m_nSegments.push_back(seed.stations);//seed has 'stations'variable,it means the size of segments;
      
    for(auto po : seed.positions){
      m_positionX.push_back(po.x());
      m_positionY.push_back(po.y());
      m_positionZ.push_back(po.z());
    }
  
    for(auto fake : seed.fakePositions){
      m_fakePositionX.push_back(fake.x());
      m_fakePositionY.push_back(fake.y());
      m_fakePositionZ.push_back(fake.z());
    }
    for(auto sp : seed.spacePoints){
      auto pos = sp->globalPosition();
      m_spacePointX.push_back(pos.x());
      m_spacePointY.push_back(pos.y());
      m_spacePointZ.push_back(pos.z());
    }



    //initilize the truth info;
    uint64_t majorityParticleId = NaNint;
    double nMajorityHits = NaNdouble;
    double t_truthHitRatio = NaNdouble;
    //float t_charge = NaNint;
   // float t_time = NaNfloat;
    float t_vx = NaNfloat;
    float t_vy = NaNfloat;
    float t_vz = NaNfloat;
    float t_px = NaNfloat;
    float t_py = NaNfloat;
    float t_pz = NaNfloat;
    float t_eta = NaNfloat;
    float t_p = NaNfloat;
    float t_pT = NaNfloat;

    if (isMC) {
      
     // Get the majority truth particle to this seed
      ATH_MSG_VERBOSE("get majority truth particle");
      identifyContributingParticles(*simData, seed.clusters, particleHitCounts);
      for (const auto& particle : particleHitCounts) {
        ATH_MSG_VERBOSE(particle.particleId << ": " << particle.hitCount << " hits");
      }

      bool foundMajorityParticle = false;
      // Get the truth particle info
      if (not particleHitCounts.empty()) {
        // Get the barcode of the majority truth particle
        majorityParticleId = particleHitCounts.front().particleId;
        nMajorityHits = particleHitCounts.front().hitCount;

	//Get the purity of every seed
        t_truthHitRatio = nMajorityHits / seed.Measurements.size();
        
	// Find the truth particle via the barcode
        auto ip = particles.find(majorityParticleId);
        if (ip != particles.end()) {
          foundMajorityParticle = true;

          const HepMC::GenParticle* particle = ip->second;
          ATH_MSG_DEBUG("Find the truth particle with barcode = " << majorityParticleId);
	  

          std::optional<const Acts::BoundTrackParameters> truthParameters
             = extrapolateToReferenceSurface(ctx, particle, targetZPosition);

          if (!truthParameters) {
            continue;
          }

	  Acts::BoundTrackParameters  boundParams = seed.get_params(targetZPosition, cov, backward);
          auto params = boundParams.parameters();
          double loc0  = params[Acts::eBoundLoc0];
          double loc1 =  params[Acts::eBoundLoc1];
          double phi =  params[Acts::eBoundPhi];
          double theta = params[Acts::eBoundTheta];
          double QoverP = params[Acts::eBoundQOverP];
          double time = params[Acts::eBoundTime];
	  
	 float track_p = boundParams.momentum().norm();
        //track_charge = truthParameters->charge();
        //track_time = truthParameters->time();
         float track_vx = boundParams.position(geoContext).x();
         float track_vy = boundParams.position(geoContext).y();
         float track_vz = boundParams.position(geoContext).z();
         float track_px = boundParams.momentum().x();
         float track_py = boundParams.momentum().y();
         float track_pz = boundParams.momentum().z();
         float track_eta = eta(boundParams.momentum().normalized());
         float track_pT = track_p * perp(boundParams.momentum().normalized());



          m_track_loc0.push_back(loc0);
          m_track_loc1.push_back(loc1);
          m_track_theta.push_back(theta);
          m_track_phi.push_back(phi);
          m_track_QoverP.push_back(QoverP);
          m_track_time.push_back(time);
          
	  
	  m_track_vx.push_back(track_vx);
          m_track_vy.push_back(track_vy);
          m_track_vz.push_back(track_vz);
          m_track_px.push_back(track_px);
          m_track_py.push_back(track_py);
          m_track_pz.push_back(track_pz);
          m_track_eta.push_back(track_eta);
          m_track_p.push_back(track_p);
          m_track_pT.push_back(track_pT);
         


	  auto truthParams = truthParameters->parameters();
          double t_loc0 = truthParams[Acts::eBoundLoc0];
          double t_loc1 = truthParams[Acts::eBoundLoc1];
          double t_phi = truthParams[Acts::eBoundPhi];
          double t_theta = truthParams[Acts::eBoundTheta];
          double t_QoverP = truthParams[Acts::eBoundQOverP];
          double t_time = truthParams[Acts::eBoundTime];
	  m_t_loc0.push_back(t_loc0);
	  m_t_loc1.push_back(t_loc1);
	  m_t_theta.push_back(t_theta);
	  m_t_phi.push_back(t_phi);
	  m_t_QoverP.push_back(t_QoverP);
	  m_t_time.push_back(t_time);

	  t_p = truthParameters->momentum().norm();
        //  t_charge = truthParameters->charge();
        //  t_time = truthParameters->time();
          t_vx = truthParameters->position(geoContext).x();
          t_vy = truthParameters->position(geoContext).y();
          t_vz = truthParameters->position(geoContext).z();
          t_px = truthParameters->momentum().x();
          t_py = truthParameters->momentum().y();
          t_pz = truthParameters->momentum().z();
          t_eta = eta(truthParameters->momentum().normalized());
          t_pT = t_p * perp(truthParameters->momentum().normalized());



        }
	else {
	  ATH_MSG_WARNING("Truth particle with barcode = " << majorityParticleId << " not found in the input collection!");
	}
      }
      if (not foundMajorityParticle) {
        ATH_MSG_WARNING("Truth particle for seed is not found!");
      }
    }
    m_majorityParticleId.push_back(majorityParticleId);
    m_nMajorityHits.push_back(nMajorityHits);
    m_t_truthHitRatio.push_back(t_truthHitRatio);
    //m_t_charge.push_back(t_charge);
    //m_t_time.push_back(t_time);
    m_t_vx.push_back(t_vx);
    m_t_vy.push_back(t_vy);
    m_t_vz.push_back(t_vz);
    m_t_px.push_back(t_px);
    m_t_py.push_back(t_py);
    m_t_pz.push_back(t_pz);
    m_t_eta.push_back(t_eta);
    m_t_p.push_back(t_p);
    m_t_pT.push_back(t_pT);
  } 
    
  m_outputTree->Fill();
  
  m_truth_p.clear();
  m_truth_vx.clear();
  m_truth_vy.clear();
  m_truth_vz.clear();
  m_truth_px.clear();
  m_truth_py.clear();
  m_truth_pz.clear();
  m_truth_pT.clear();
//  m_truth_pdg.clear();
  m_truth_barcode.clear();
  m_truth_isFiducial.clear();
  m_truth_theta.clear();
  m_truth_phi.clear(); 
  m_truth_eta.clear();
  m_targetZPosition=0.0;
  m_nseeds=0;
  m_nClusters.clear();
  m_nMeasurements.clear();
  m_nSegments.clear();

  m_positionX.clear();
  m_positionY.clear();
  m_positionZ.clear();
  
  m_spacePointX.clear();
  m_spacePointY.clear();
  m_spacePointZ.clear();
  
  m_fakePositionX.clear();
  m_fakePositionY.clear();
  m_fakePositionZ.clear();

  m_nMajorityHits.clear();
  m_majorityParticleId.clear();
  m_t_truthHitRatio.clear();
  //m_t_charge.clear();
  //m_t_time.clear();

  m_track_loc0.clear();
  m_track_loc1.clear();
  m_track_theta.clear();
  m_track_phi.clear();
  m_track_QoverP.clear();
  m_track_time.clear();
  m_track_vx.clear();
  m_track_vy.clear();
  m_track_vz.clear();
  m_track_px.clear();
  m_track_py.clear();
  m_track_pz.clear();
  m_track_p.clear();
  m_track_pT.clear();
  m_track_eta.clear();

  m_t_loc0.clear();
  m_t_loc1.clear();
  m_t_theta.clear();
  m_t_phi.clear();
  m_t_QoverP.clear();
  m_t_time.clear();

  m_t_vx.clear();
  m_t_vy.clear();
  m_t_vz.clear();
  m_t_px.clear();
  m_t_py.clear();
  m_t_pz.clear();
  m_t_eta.clear();

  return StatusCode::SUCCESS;  
}


std::optional<const Acts::BoundTrackParameters> RootSeedWriterTool::extrapolateToReferenceSurface(
    const EventContext& ctx, const HepMC::GenParticle* particle, const double& targetZ) const {
  const HepMC::FourVector &vertex = particle->production_vertex()->position();
  const HepMC::FourVector &momentum = particle->momentum();

  // The coordinate system of the Acts::PlaneSurface is defined as
  // T = Z = normal, U = X x T = -Y, V = T x U = x
  auto startSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3(0, 0, vertex.z()), Acts::Vector3(0, 0, 1));
  auto targetSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3(0, 0, targetZ), Acts::Vector3(0, 0, -1));
 
  Acts::GeometryContext gctx;
  Acts::Vector3 p3(vertex.x(),vertex.y(),vertex.z());
  auto lpResult = startSurface->globalToLocal(gctx, p3);
  if (not lpResult.ok()) {
    throw std::runtime_error("globalToLocal transformation failed. This should not happen");
  }
  auto lPosition = lpResult.value();
  
  Acts::BoundVector params = Acts::BoundVector::Zero();
  params[Acts::eBoundLoc0] = lPosition.x();
  params[Acts::eBoundLoc1] = lPosition.y();
  params[Acts::eBoundPhi] = momentum.phi();
  params[Acts::eBoundTheta] = momentum.theta();
  // FIXME get charge of HepMC::GenParticle, the following does not work, e.g. for pions
  double charge = particle->pdg_id() > 0 ? -1 : 1;
  double MeV2GeV = 1e-3;
  params[Acts::eBoundQOverP] = charge / (momentum.rho() * MeV2GeV);
  params[Acts::eBoundTime] = vertex.t();
  Acts::BoundTrackParameters startParameters(std::move(startSurface), params,  std::nullopt, Acts::ParticleHypothesis::muon());
  std::optional<const Acts::BoundTrackParameters> targetParameters =
      m_extrapolationTool->propagate(ctx, startParameters, *targetSurface);
  return targetParameters;
}
