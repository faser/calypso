from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from AthenaPython import PyAthena
from AthenaPython.PyAthena import StatusCode, McEventCollection, CLHEP
from AthenaCommon.SystemOfUnits import cm, m, GeV
import ROOT
from math import sqrt

try:
    from AthenaPython.PyAthena import HepMC3  as HepMC
except ImportError:
    from AthenaPython.PyAthena import HepMC   as HepMC

class ConversionFilter(PyAthena.Alg):
    def __init__(self, name="SimFilter", InputMCEventKey="TruthEvent", minConversionEnergy = None,
                 maxRadius = None, muonEnergyLoss = None):

        super(ConversionFilter,self).__init__(name=name)

        self.InputMCEventKey = InputMCEventKey
        self.minConversionEnergy = minConversionEnergy
        self.maxRadius = maxRadius
        self.muonEnergyLoss = muonEnergyLoss

        self.zEmulsionEnd = -1.6 * m

        return

    def findConversionFromMuon(self, evt):

        for i, p in enumerate(evt.particles):

            # Check incoming particle (muon) is within radius to
            # avoid those conversions from muons coming in to magnet
            if i == 1 and self.maxRadius is not None:
                pvtx = p.production_vertex()
                if pvtx:
                    ppos = pvtx.position()
                    if sqrt(ppos.x()**2 + ppos.y()**2) > self.maxRadius:
                        return False
                
            # Find photon
            if not abs(p.pdg_id()) == 22: continue

            # Decaying to 2 children
            ovtx = p.end_vertex()
            if not ovtx: continue
            if ovtx.particles_out_size() != 2: continue

            # That are e+e- pair
            children = list(ovtx.particles_out)
            if not (abs(children[0].pdg_id()) == 11 and children[0].pdg_id() == -children[1].pdg_id()): continue        
            self.msg.debug("Found Photon conversion")

            # Which comes from a single mother
            ivtx = p.production_vertex()
            if not ivtx: continue
            if ivtx.particles_in_size() != 1: continue
            mother = list(ivtx.particles_in)[0]

            # That is a muon (allowing for another photon inbetween)
            isFromMuon = False
            if abs(mother.pdg_id()) == 13:
                isFromMuon = True
            elif abs(mother.pdg_id()) == 22:
                ivtx = mother.production_vertex()
                if ivtx and ivtx.particles_in_size() == 1:
                    gran = list(ivtx.particles_in)[0]
                    if abs(gran.pdg_id()) == 13:
                        isFromMuon = True        

            if isFromMuon:
                # Optionally require a certain energy for at least one of the e+e- pair
                if (self.minConversionEnergy is None
                    or children[0].momentum().e() > self.minConversionEnergy
                    or children[1].momentum().e() > self.minConversionEnergy):                    

                    self.msg.debug("... from muon")
                    self.setFilterPassed(True)
                    return True

        return False

    def findLargeELossMuon(self, evt):

        lastMuonE = None
        firstMuonE = None
        
        for p in evt.particles:

            # Find muons
            if abs(p.pdg_id()) != 13: continue

            # With a vertex inside the specified radius
            pvtx = p.production_vertex()
            if not pvtx: continue
            ppos = pvtx.position()

            if self.maxRadius is not None and sqrt(ppos.x()**2 + ppos.y()**2) > self.maxRadius:
                continue

            # Find the final muon produced in the emulsion
            if ppos.z() < self.zEmulsionEnd:
                firstMuonE = p.momentum().e()

            # Find the final muon
            lastMuonE = p.momentum().e()

        # Look for E loss between end of emulsion and final
        if lastMuonE is not None and firstMuonE is not None:
            if firstMuonE - lastMuonE > self.muonEnergyLoss:
                self.msg.debug(f"Found muon with E loss = {firstMuonE - lastMuonE}")
                self.setFilterPassed(True)
                return True

        return False
            
    

    def execute(self):
        self.msg.debug(f"Executing {self.getName()}")

        self.msg.debug(f"Reading {self.InputMCEventKey}")
        evt = self.evtStore[self.InputMCEventKey][0]
        
        self.setFilterPassed(False)

        if self.muonEnergyLoss:
            self.findLargeELossMuon(evt)
        else:
            self.findConversionFromMuon(evt)

        return StatusCode.Success
